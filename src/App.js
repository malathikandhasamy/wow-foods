import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import MenuView from './views/MenuView';
import CartView from './views/CartView';
import { Container } from 'reactstrap';



function App() {
    return (

        <BrowserRouter>
            <nav className="navbar navbar-light bg-danger" >
                <Container>
                    <a className="navbar-brand" href="#">
                        <img src="/assets/images/logo-02.png" height="30" className="d-inline-block align-top pr-2" alt="" />

                    </a>
                </Container>
            </nav>
            <Switch>
                <Route path={"/menu"} component={MenuView} />
                <Route path={"/cart"} component={CartView} />
                <Redirect from={"/"} to={"/menu"} />
            </Switch>
        </BrowserRouter>
    );
}

export default App;
