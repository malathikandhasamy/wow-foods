import React, { useEffect, useState } from 'react';
import { Container, Row, Col, Button, Card, ButtonGroup, ListGroup, ListGroupItem } from 'reactstrap'
import axios from 'axios';
const MenuView = (props) => {
    let temp = JSON.parse(localStorage.getItem('cartList'))
    const [productList, setProductList] = useState([])
    const [cartList, setCartList] = useState(temp || [])
    const [isLoading, setIsLoding] = useState(false)
    useEffect(() => {
        setIsLoding(true)
        axios.get('https://run.mocky.io/v3/9d71cb03-a9f9-4d70-bae2-9d3adaa1cfe7').then(response => response.data).then(res => {
            // console.log(res)
            setProductList(res)
            setIsLoding(false)
        })
    }, [])
    useEffect(() => {
        localStorage.setItem("cartList", JSON.stringify(cartList))
    }, [cartList])
    let addToCart = (item) => {
        // console.log(id)
        let obj = {
            ...item,
            quantity: 1
        }
        setCartList([...cartList, obj])

    }

    let removeQuantity = (inx) => {
        if (cartList[inx].quantity > 1) {
            cartList[inx].quantity = cartList[inx].quantity - 1;
            setCartList([...cartList])
        }
        else {
            cartList.splice(inx, 1)
            setCartList([...cartList])
        }
    }

    let addQuantity = (inx) => {
        cartList[inx].quantity = cartList[inx].quantity + 1;
        setCartList([...cartList])
    }
    let navicateCart = () => {
        props.history.push('/cart')
    }

    return (
        <div>
            <Container>
                <Row>
                    <Col sm="12" md={{ size: 8 }}>
                        <h3><b>Menu List</b></h3>
                        <hr></hr>
                        {
                            isLoading && <p className="text-center">Please Wait..</p>
                        }
                        {productList.map((item, inx) => {
                            let cartIndex = cartList.findIndex(f => f.id == item.id);
                            return (
                                <Card body className="flex-row align-items-center justify-content-between mb-1" key={inx}>
                                    <div>
                                        <p className="mb-0">{item.item_name}</p>
                                        <i className="fas fa-rupee-sign pr-1"></i>{item.price}
                                    </div>
                                    <div>
                                        {cartIndex == -1 ?
                                            (
                                                <Button color="danger" outline onClick={() => addToCart(item)}>
                                                    Add <i className="fas fa-plus"></i>
                                                </Button>
                                            ) : (
                                                <ButtonGroup size="sm">
                                                    <Button color="danger" onClick={() => removeQuantity(cartIndex)}>
                                                        <i className="fas fa-minus "></i>
                                                    </Button>
                                                    <Button outline color="">{cartList[cartIndex].quantity}</Button>
                                                    <Button color="danger" onClick={() => addQuantity(cartIndex)}>
                                                        <i className="fas fa-plus"></i>
                                                    </Button>
                                                </ButtonGroup>
                                            )}
                                    </div>
                                </Card>
                            )
                        })}
                    </Col>
                    <Col sm="12" md={{ size: 4 }}>
                        <h3><b>Cart</b></h3>
                        <hr />
                        <ListGroup className="mb-2">

                            {cartList.map((item, inx) => {
                                return (
                                    <ListGroupItem className="d-flex align-items-center justify-content-between" key={inx}>
                                        <div>
                                            <p className="mb-1">{item.item_name}</p>
                                            <ButtonGroup size="sm">
                                                <Button color="danger" onClick={() => removeQuantity(inx)}>
                                                    <i className="fas fa-minus "></i>
                                                </Button>
                                                <Button outline color="">{item.quantity}</Button>
                                                <Button color="danger" onClick={() => addQuantity(inx)}>
                                                    <i className="fas fa-plus"></i>
                                                </Button>
                                            </ButtonGroup>
                                        </div>
                                        <div>
                                            <i className="fas fa-rupee-sign pr-1"></i>{item.quantity * item.price}
                                        </div>
                                    </ListGroupItem>
                                )
                            })}
                            <ListGroupItem className="d-flex align-items-center justify-content-between">
                                <div>
                                    <b className="m-0">SubTotal</b>
                                </div>
                                <div>
                                    <i className="fas fa-rupee-sign pr-1"></i>{cartList.reduce((total, item) => {
                                        total = total + (item.quantity * item.price)
                                        return total
                                    }, 0)}
                                </div>
                            </ListGroupItem>
                        </ListGroup>
                        <Button color="danger" block size="lg" onClick={() => navicateCart()}>Checkout</Button>
                    </Col>
                </Row>

            </Container>



        </div>
    )
}

export default MenuView