
import { useEffect, useState } from "react";
import { ButtonGroup, Container, ListGroupItem, Button, ListGroup } from "reactstrap"
import Swal from 'sweetalert2'



const CartView = (props) => {
    let temp = JSON.parse(localStorage.getItem('cartList'))
    const [cartList, setCartList] = useState(temp);
    const [totalPrice, setTotalPrice] = useState(0);
    const [discount, setDiscount] = useState(0)


    useEffect(() => {
        localStorage.setItem("cartList", JSON.stringify(cartList))
        let totalAmount = cartList.reduce((total, item) => {
            total = total + (item.quantity * item.price)
            return total
        }, 0)
        setTotalPrice(totalAmount)
        if (totalAmount > 100 && totalAmount < 500) {
            setDiscount(0.10)
        }
        else if (totalAmount > 500) {
            setDiscount(0.20)
        }
        else {
            setDiscount(0)
        }
    }, [cartList])

    let removeQuantity = (inx) => {
        if (cartList[inx].quantity > 1) {
            cartList[inx].quantity = cartList[inx].quantity - 1;
            setCartList([...cartList])
        }
        else {
            cartList.splice(inx, 1)
            setCartList([...cartList])
        }
    }

    let addQuantity = (inx) => {
        cartList[inx].quantity = cartList[inx].quantity + 1;
        setCartList([...cartList])
    }

    let navicateMenu = () => {
        props.history.push('/menu')
    }
    let clearCart = () =>{
        setCartList([])
    }

    return (
        <section>
            <Container>
                <div className="d-flex align-items-center justify-content-between" >
                    <h3><b>Cart</b></h3>
                    <div className="d-flex">
                    <div className="text-danger link" onClick={() => navicateMenu()}> <i className="fas fa-caret-left pr-1"></i>Back to Menu</div>
                    <span className="mx-3">|</span>
                    <div className="text-danger link" onClick={() => clearCart()}> <i className="fas fa-times pr-1"></i>Clear Cart</div>

                    </div>
                </div>
                <hr />
                <ListGroup className="mb-2">

                    {cartList.map((item, inx) => {
                        return (
                            <ListGroupItem className="d-flex align-items-center justify-content-between" key={inx}>
                                <div>
                                    <p className="mb-1">{item.item_name}</p>
                                    <ButtonGroup size="sm">
                                        <Button color="danger" onClick={() => removeQuantity(inx)}>
                                            <i className="fas fa-minus "></i>
                                        </Button>
                                        <Button outline color="">{item.quantity}</Button>
                                        <Button color="danger" onClick={() => addQuantity(inx)}>
                                            <i className="fas fa-plus"></i>
                                        </Button>
                                    </ButtonGroup>
                                </div>
                                <div>
                                    <i className="fas fa-rupee-sign pr-1"></i>{item.quantity * item.price}
                                </div>
                            </ListGroupItem>
                        )
                    })}
                    <ListGroupItem className="d-flex align-items-center justify-content-between">
                        <div>
                            <b className="m-0">SubTotal</b>
                        </div>
                        <div>
                            <i className="fas fa-rupee-sign pr-1"></i>{totalPrice}
                        </div>
                    </ListGroupItem>

                    {discount != 0 && (
                        <ListGroupItem className="d-flex align-items-center justify-content-between">
                            <div>
                                <b className="m-0">Discount</b> ({discount * 100}%)
                            </div>
                            <div>
                                <i className="fas fa-rupee-sign pr-1"></i>{(totalPrice * discount).toFixed(2)}
                            </div>
                        </ListGroupItem>

                    )}
                    <ListGroupItem className="d-flex align-items-center justify-content-between">
                        <div>
                            <b className="m-0">Total Amount</b>
                        </div>
                        <div>
                            <i className="fas fa-rupee-sign pr-1"></i>{(totalPrice - (totalPrice * discount)).toFixed(2)}
                        </div>
                    </ListGroupItem>
                </ListGroup>
                <Button color="danger" block size="lg" onClick={() => Swal.fire("Thank You", "Your Order Placed Successfully", "success").then(()=>{
                    clearCart()
                    navicateMenu()
                })}>Submit</Button>
            </Container>
        </section>
    )
}

export default CartView